#
# See LICENSE for licensing details.
#
# Copy me if you can.
# by 20h
#

import io

def parse(mdstr, topic="", subtopic=""):
	"""
	Parse wiki markdown into an dict / array tree.
	"""

	page = {}
	page["topic"] = None
	page["tree"] = {}
	page["description"] = []

	lastlevel = 0
	depthlist = [page["description"]]
	for line in io.StringIO(mdstr):
		line = line.rstrip()
		if len(line) == 0:
			continue

		if line.startswith("# ") and page["topic"] == None:
			page["topic"] = line[2:]
			continue
		elif line[0] == '#':
			while line[0] == '#':
				line = line[1:]
			line = line.lstrip()

			page["tree"][line] = []
			depthlist = [page["tree"][line]]
			continue
		else:
			level = 0
			while line[0] == '\t':
				level += 1
				line = line[1:]
			if line.startswith("* "):
				line = line[2:]
			line = line.lstrip()

			if level == lastlevel:
				depthlist[-1].append(line)
			elif level < lastlevel:
				leveljump = lastlevel - level
				if len(depthlist) > leveljump:
					depthlist = depthlist[:-leveljump]
				depthlist[-1].append(line)
			elif level > lastlevel:
				linea = [line]
				depthlist[-1].append(linea)
				depthlist.append(linea)
			lastlevel = level
			continue
	
	return page

