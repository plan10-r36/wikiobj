#!/usr/bin/env python
# coding=utf-8
#
# Copy me if you can.
# by 20h
#

try:
	from setuptools import setup
except ImportError:
	from distutils.core import setup

setup(
	name='wikiobj',
	version='0.1.0',

	py_modules=['wikiobj'],
	packages=['wikiobj'],

	provides=['wikiobj'],
	requires=[],
	platforms=['all'],

	author='Christoph Lohmann',
	author_email='20h@r-36.net',
	maintainer='Christoph Lohmann',
	maintainer_email='20h@r-36.net',
	url='http://git.r-36.net/wikiobj',
	description='Wikiobj is a simple parser for markdown wiki pages.',
	long_description=open("README.md").read(),
	license='GPLv3',
	classifiers=[
		'Environment :: Console',
		'Intended Audience :: End Users/Desktop',
		'Operating System :: OS Independent',
		'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
		'Programming Language :: Python',
	],
)

